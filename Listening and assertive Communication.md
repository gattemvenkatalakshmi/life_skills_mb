### Question 1
**Active Listening Strategies:**
1. **Give Full Attention:** Focus on the speaker without distractions, showing genuine interest.
2. **Paraphrasing:** Repeat what you heard in your own words to confirm understanding.
3. **Clarifying:** Ask questions to ensure you comprehend the message accurately.
4. **Non-Verbal Cues:** Use body language and gestures to convey attentiveness and understanding.
5. **Empathy:** Understand and acknowledge the speaker's feelings, demonstrating emotional connection.
6. **Summarizing:** Recap the key points to show you've grasped the main ideas.

### Question 2
**Key Points of Reflective Listening (Fisher's Model):**
Reflective Listening involves mirroring body language, emotions, and verifying messages. It's crucial in critical situations, business, and personal relationships. The key points are to mirror non-verbal cues, validate emotions, and ensure accurate understanding in situations like air traffic control, military missions, business meetings, and personal conversations.

### Question 3
**Obstacles in Listening Process:**
1. **Distractions:** External factors divert attention.
2. **Prejudice:** Preconceived notions or biases affect understanding.
3. **Assumption:** Assuming you know what will be said without listening.
4. **Selective Listening:** Focusing only on parts of the message.
5. **Judgment:** Forming opinions before hearing the complete message.
6. **Lack of Empathy:** Inability to understand or share another person's feelings.

### Question 4
**Improving Listening:**
1. **Practice Active Listening Techniques:** Develop the skills mentioned earlier.
2. **Remove Distractions:** Create a conducive environment for listening.
3. **Be Open-Minded:** Avoid forming judgments prematurely.
4. **Ask Clarifying Questions:** Seek additional information for better understanding.
5. **Show Empathy:** Understand and acknowledge the speaker's perspective.

### Question 5
**Switch to Passive Communication:**
In day-to-day life, one might switch to passive communication when avoiding confrontation, staying silent to maintain harmony, or when feeling overwhelmed and not expressing thoughts or opinions.

### Question 6
**Switch into Aggressive Communication:**
Aggressive communication may emerge when feeling threatened, frustrated, or seeking dominance. It involves expressing opinions forcefully, dominating conversations, or using harsh language.

### Question 7
**Switch into Passive Aggressive Communication:**
Passive-aggressive communication occurs when suppressing feelings outwardly but expressing dissatisfaction indirectly, often through sarcasm, gossiping, taunts, or giving the silent treatment.

### Question 8
**Making Communication Assertive:**
1. **Express Clearly:** Clearly state thoughts and feelings without aggression.
2. **Use "I" Statements:** Share personal perspectives without blaming others.
3. **Active Listening:** Demonstrate understanding by acknowledging others' views.
4. **Set Boundaries:** Clearly define limits and expectations.
5. **Stay Calm:** Manage emotions to avoid aggressive reactions.
6. **Problem-Solving:** Focus on finding solutions rather than dwelling on problems.
